var searchData=
[
  ['config1_2',['config1',['../board_8c.html#a6ac52c0047c2794ee1b9e01770b3b3a6',1,'config1(GamePlate *gt):&#160;board.c'],['../board_8h.html#a6ac52c0047c2794ee1b9e01770b3b3a6',1,'config1(GamePlate *gt):&#160;board.c']]],
  ['config2_3',['config2',['../board_8c.html#ac69ec3e8984a0b68ac341cfac68f400f',1,'config2(GamePlate *gt):&#160;board.c'],['../board_8h.html#ac69ec3e8984a0b68ac341cfac68f400f',1,'config2(GamePlate *gt):&#160;board.c']]],
  ['config3_4',['config3',['../board_8c.html#afcf06e6b5b033922ad45c38ef7571465',1,'config3(GamePlate *gt):&#160;board.c'],['../board_8h.html#afcf06e6b5b033922ad45c38ef7571465',1,'config3(GamePlate *gt):&#160;board.c']]],
  ['config4_5',['config4',['../board_8c.html#a9c16fbd38c1b375344c8c8b3779e83c8',1,'config4(GamePlate *gt):&#160;board.c'],['../board_8h.html#a9c16fbd38c1b375344c8c8b3779e83c8',1,'config4(GamePlate *gt):&#160;board.c']]],
  ['config5_6',['config5',['../board_8c.html#adcaa92b3ee0791c5d92d57ef83ca123d',1,'config5(GamePlate *gt):&#160;board.c'],['../board_8h.html#adcaa92b3ee0791c5d92d57ef83ca123d',1,'config5(GamePlate *gt):&#160;board.c']]],
  ['count_5falive_5fcircular_7',['count_alive_circular',['../board_8c.html#ac8d36fc1cbc4dce87598d65958207f3a',1,'count_alive_circular(GamePlate *gt, int column, int row):&#160;board.c'],['../board_8h.html#ac8d36fc1cbc4dce87598d65958207f3a',1,'count_alive_circular(GamePlate *gt, int column, int row):&#160;board.c']]],
  ['count_5falive_5fclipped_8',['count_alive_clipped',['../board_8c.html#a8fbe9c69ddfe26b2ff02abb4ad985355',1,'count_alive_clipped(GamePlate *gt, int column, int row):&#160;board.c'],['../board_8h.html#a8fbe9c69ddfe26b2ff02abb4ad985355',1,'count_alive_clipped(GamePlate *gt, int column, int row):&#160;board.c']]]
];
