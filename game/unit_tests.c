#include "boardlib/board.h"
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <CUnit/Console.h>

void test_counting_function(){
    GamePlate* gt = new_game_tray(5,5,0);
    set_value(gt, 1, 5, 1);
    set_value(gt, 1, 4, 1);
    set_value(gt, 1, 5, 2);
    set_value(gt, 1, 1, 1);

    CU_ASSERT_EQUAL(count_alive_clipped(gt,5,1), 2);
    CU_ASSERT_EQUAL(count_alive_circular(gt,5,1), 3);
}

void test_step_function(){
    GamePlate* gt1 = new_game_tray(5,5,0);
    set_value(gt1, 1, 5, 1);
    set_value(gt1, 1, 5, 2);
    set_value(gt1, 1, 1, 1);

    GamePlate* gt2 = new_game_tray(5,5,1);
    set_value(gt2, 1, 5, 1);
    set_value(gt2, 1, 5, 2);
    set_value(gt2, 1, 1, 1);

    GamePlate* result = step(gt1);
    CU_ASSERT_EQUAL(get_value(result, 5, 1), 0);
    result = step(gt2);
    CU_ASSERT_EQUAL(get_value(result, 5, 1), 1);
}

int main(){
    if(CUE_SUCCESS != CU_initialize_registry()){return CU_get_error();}

    CU_pSuite counter = CU_add_suite("Testing counting function", NULL, NULL);
    CU_ADD_TEST(counter, test_counting_function);

    CU_pSuite logic = CU_add_suite("Testing Game Logic", NULL, NULL);
    CU_ADD_TEST(logic, test_step_function);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    return CU_get_number_of_failures();
}