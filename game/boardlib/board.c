#include "board.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

GamePlate* new_game_tray(int width, int height, int mode){
    if(width<2 || height<2){printf("The dimensions must be greater or equal than two!\n"); return NULL;}
    if(mode!=1 && mode!=0){printf("The mode code should be either 1 or 0!\n"); return NULL;}
    GamePlate *gt=(GamePlate*)malloc(sizeof(GamePlate));
    (*gt).n_column=width;
    (*gt).n_row=height;
    (*gt).mode=mode;
    (*gt).pl=(int*)calloc(gt->n_row*gt->n_column,sizeof(int));
    return gt;
}

void delete_game_tray(GamePlate* gt){
    free(gt->pl);
    free(gt);
}

int get_value(GamePlate* gt, int column, int row){
    if(gt==NULL){printf("Invalid Game Plate reference! Can not get the value!\n"); return -1;}
    return *(gt->pl+(row-1)*gt->n_column+(column-1));
}

void set_value(GamePlate* gt, int value, int column, int row){
    if(gt==NULL){printf("Invalid Game Plate reference! Impossible to set any value!\n"); return;}
    *(gt->pl+(row-1)*gt->n_column+(column-1))=value;
}

void ShowGame(GamePlate* gt){
    if(gt==NULL){printf("Invalid Game Plate reference! No plate to be printed!\n"); return;}

    for(int i=0;i<gt->n_row;i++){
        for(int j=0;j<gt->n_column;j++){
            int value=get_value(gt,j+1,i+1);
            if(value==0){printf("\033[30m%c\033[0m ",254);}
            else if(value==1){printf("%c ",254);}
        }
        printf("\n");
    }
}

void initiate(GamePlate* gt){
    srand(time(0));
    for(int i = 0; i <gt->n_row; i++){
        for (int j = 0; j<gt->n_column;j++){
            set_value(gt, rand()%2, j+1, i+1);
        }
    }
}

GamePlate* step(GamePlate* gt){
    if(gt==NULL){printf("Invalid Game Plate reference! Nothing to be done!\n"); return -1;}
    GamePlate* new = new_game_tray(gt->n_column, gt->n_row, gt->mode);
    
    for(int i=0;i<gt->n_row;i++){
        for(int j=0;j<gt->n_column;j++){
            int n = (gt->mode==1) ? count_alive_circular(gt,j+1,i+1) : count_alive_clipped(gt,j+1,i+1);
            int state = get_value(gt, j+1, i+1);
            if (state==0 && n==3){set_value(new, 1, j+1, i+1);}
            else if (state==1 && n!=3 && n!=2){set_value(new, 0, j+1, i+1);}
            else{set_value(new, state, j+1, i+1);}
        }
    }
    return new;
}

int count_alive_clipped(GamePlate* gt, int column, int row){
    int n=0;
    if(row!=1){if(get_value(gt, column, row-1)==1){n+=1;}}
    if(row!=gt->n_row){if(get_value(gt, column, row+1)==1){n+=1;}}
    if(column!=1){if(get_value(gt, column-1, row)==1){n+=1;}}
    if(column!=gt->n_column){if(get_value(gt, column+1, row)==1){n+=1;}}
    if(column!=1 && row!=1){if(get_value(gt, column-1, row-1)==1){n+=1;}}
    if(column!=gt->n_column && row!=gt->n_row){if(get_value(gt, column+1, row+1)==1){n+=1;}}
    if(column!=1 && row!=gt->n_row){if(get_value(gt, column-1, row+1)==1){n+=1;}}
    if(column!=gt->n_column && row!=1){if(get_value(gt, column+1, row-1)==1){n+=1;}}
    return n;
}

int count_alive_circular(GamePlate* gt, int column, int row){
    int sum=0;
    for(int i = -1; i <=1; i++){
        for(int j = -1; j<=1;j++){
            int x = column+j;
            int y = row+i;
            if(x==0){x=gt->n_column;}
            if(x==gt->n_column+1){x=1;}
            if(y==0){y=gt->n_row;}
            if(y==gt->n_row+1){y=1;}
            if(get_value(gt,x,y)==1){
                sum+=1;
            }
        }
    }

    sum-=get_value(gt,column,row);
    return sum;
}

void config1(GamePlate* gt){
    set_value(gt, 1, 5, 1);
    set_value(gt, 1, 5, 2);
    set_value(gt, 1, 5, 3);
    set_value(gt, 1, 5, 7);
    set_value(gt, 1, 5, 8);
    set_value(gt, 1, 5, 9);
    set_value(gt, 1, 1, 5);
    set_value(gt, 1, 2, 5);
    set_value(gt, 1, 3, 5);
    set_value(gt, 1, 7, 5);
    set_value(gt, 1, 8, 5);
    set_value(gt, 1, 9, 5);
}
void config2(GamePlate* gt){
    set_value(gt, 1, 4, 4);
    set_value(gt, 1, 3, 5);
    set_value(gt, 1, 3, 6);
    set_value(gt, 1, 3, 7);
    set_value(gt, 1, 4, 7);
    set_value(gt, 1, 5, 7);
    set_value(gt, 1, 6, 7);
    set_value(gt, 1, 7, 6);
    set_value(gt, 1, 7, 4);
}
void config3(GamePlate* gt){
    set_value(gt, 1, 10, 7);
    set_value(gt, 1, 10, 8);
    set_value(gt, 1, 10, 9);
    set_value(gt, 1, 10, 10);
    set_value(gt, 1, 10, 11);
    set_value(gt, 1, 10, 12);
    set_value(gt, 1, 10, 13);
    
}
void config4(GamePlate* gt){
    set_value(gt, 1, 6, 7);
    set_value(gt, 1, 7, 6);
    set_value(gt, 1, 7, 8);
    set_value(gt, 1, 8, 7);
    set_value(gt, 1, 8, 8);
    set_value(gt, 1, 9, 9);
    set_value(gt, 1, 9, 10);
    set_value(gt, 1, 10, 9);
    set_value(gt, 1, 11, 10);
    set_value(gt, 1, 11, 12);
    set_value(gt, 1, 13, 12);
    set_value(gt, 1, 13, 14);
    set_value(gt, 1, 14, 15);
    set_value(gt, 1, 15, 14);
    set_value(gt, 1, 15, 15);
}
void config5(GamePlate* gt){
    set_value(gt, 1, 8, 10);
    set_value(gt, 1, 8, 11);
    set_value(gt, 1, 9, 9);
    set_value(gt, 1, 9, 11);
    set_value(gt, 1, 10, 9);
    set_value(gt, 1, 10, 11);
    set_value(gt, 1, 11, 10);
    set_value(gt, 1, 11, 11);
}