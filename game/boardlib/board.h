#ifndef _BOARD_H_
#define _BOARD_H_

#include <stdio.h> 

/*!< Defines GameTray structure */
typedef struct GameTray{ 
    int n_row;/*!< Defines the number of rows */
    int n_column;/*!< Defines the number of columns */
    int *pl;/*!< Defines the plate matrix */
    int mode;/*!< Defines mode of the gametray (clipped or circular) */
}GamePlate;

GamePlate* new_game_tray(int width, int height, int mode);/*!< Function which creates game plate */
void initiate(GamePlate* gt); /*!< Function which randomly initialize each cell */
GamePlate* step(GamePlate* gt);/*!< Function which produces the next step of generation */
int count_alive_clipped(GamePlate* gt, int column, int row);/*!< Function which counts number of alive neigbours of specified cell (clipped) */
int count_alive_circular(GamePlate* gt, int column, int row); /*!< Function which counts number of alive neigbours of specified cell circularly */
void delete_game_tray(GamePlate* gt); /*!< Function which deletes everything from the game tray */
int get_value(GamePlate* gt, int column, int row);/*!< Function which gets the value from specified cell */
void set_value(GamePlate* gt, int value, int column, int row); /*!< Function which sets the value to specified cell */
void ShowGame(GamePlate* gt);/*!< Function which prints the game tray*/
void config1(GamePlate* gt);/*!< Function which produces the first configuration of the game */
void config2(GamePlate* gt);/*!< Function which produces the second configuration of the game */
void config3(GamePlate* gt);/*!< Function which produces the third configuration of the game */
void config4(GamePlate* gt); /*!< Function which produces the fourth configuration of the game */
void config5(GamePlate* gt); /*!< Function which produces the fifth configuration of the game */
#endif