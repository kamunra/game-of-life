#include <stdio.h>
#include <unistd.h>
#include "boardlib/board.h"
#include <stdlib.h>
#include <time.h>

int main(){ /*!<Runs the game */
    int w,h;
    int m;
    int c = 0;
    printf("Choose the start configuration of the game (from 1 to 5 || 0 for random seed)\n");
    scanf("%d",&c);
    GamePlate* gt;
    switch (c)
    {
    case 1:{
        gt = new_game_tray(9,9,0);
        config1(gt);
        break;
    }
    case 2:{
        gt = new_game_tray(10,10,1);
        config2(gt);
        break;
    }
    case 3:{
        gt = new_game_tray(20,20,1);
        config3(gt);
        break;
    }
    case 4:{
        gt = new_game_tray(20,20,1);
        config4(gt);
        break;
    }
    case 5:{
        gt = new_game_tray(18,20,1);
        config5(gt);
        break;
    }
    case 0:{
        printf("choose the width and height of gameplate\n");
        scanf("%d",&w);
        scanf("%d",&h);
        printf("choose the mode (0 - clipped, 1-circular)\n");
        scanf("%d",&m);
        gt = new_game_tray(w,h,m);
        initiate(gt);
        break;
    }
    }
    while(1){
        printf("\033[2J");
        printf("\033[H");
        ShowGame(gt);
        usleep(200000);
       gt = step(gt);
   }
    return 0;
}
